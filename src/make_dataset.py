import pandas as pd
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
import spacy
import re
import pickle as pk

my_data = pd.read_csv('/builds/ai9155921/tweetssentimentanalysispipeline/data/row/data.csv', encoding = "ISO-8859-1", header=None)

my_data.columns = ['target', 'id', 'date', 'flag', 'user', 'text']
columns_to_drop = ['id', 'date', 'flag', 'user']
my_data.drop(columns_to_drop,axis=1, inplace=True)

#my_data.to_pickle('../data/processed/my_data.pkl')
my_data.to_pickle('/builds/ai9155921/tweetssentimentanalysispipeline/data/processed/my_data.pkl')